## Getting Started

1. Clone the git repository

   ```
   git clone https://gitlab.com/shahpankaj981/workers-safety-measure
   ```

   

2. Installation

   ```
   pip3 install -r requirements.txt
   ```

   

3. Run

   ```
   python3 video_demo.py --video_file_name="[FILE_NAME]" --camera_id="[CAMERA_ID]"
   ```

   where,

   `[FILE_NAME]` = name of video file (OPTIONAL, default is video file named "input.mp4")

   `[CAMERA_ID]` = id of camera to use (OPTIONAL, if not given, tries to work on video)
   
 
   
   
## For Raspberry Pi

1. Download the zip file from the link below:

   *https://drive.google.com/file/d/151xJp9JshgJke-VmMwZeIeKz9zkpfaVJ/view?usp=sharing*

2. Extract the image file from the downloaded zip file.
3. Make an SD card bootable using the extracted image file.


# To use Yolo tiny Model

   ```
   python3 video_demo.py --video_file_name="[FILE_NAME]" --camera_id=[None]
   ```

   where,

   `[FILE_NAME]` = name of video file (OPTIONAL, default is video file named "input.mp4")

   `[CAMERA_ID]` = Boolean (OPTIONAL, if not given, tries to work on video)
   

## Link to Dataset in YOLO format 

   ```
   https://drive.google.com/file/d/1nLAY4qakgXt_Moz5zN8C6sg9WU8DuNYx/view?usp=sharing
   ```