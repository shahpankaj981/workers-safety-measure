import cv2
import RPi.GPIO as GPIO
#GPIO.setmode(GPIO.BCM)
#GPIO.setup(2, GPIO.OUT, initial=0)

# USAGE
# python yolo_video.py --input videos/airport.mp4 --output output/airport_output.avi --yolo yolo-coco


import time
import os
import cv2
import numpy as np
from picamera.array import PiRGBArray
from picamera import PiCamera
import argparse

yolo_label = './package/obj.names'
    # load the COCO class labels our YOLO model was trained on
    # labelsPath = os.path.sep.join(yolo_label)
LABELS = open(yolo_label).read().strip().split("\n")
# construct the argument parse and parse the arguments
np.random.seed(42)
COLORS = np.random.randint(0, 255, size=(len(LABELS), 3),
	dtype="uint8")

def picam_detection(net, ln, confidence_a, threshold_a, LABELS):
    camera = PiCamera()
    camera.resolution = (320, 240)
    camera.framerate = 30

    rawCapture = PiRGBArray(camera, size=(320, 240))

    time.sleep(1)

    W = 320
    H = 240
    detect = False

    # loop over frames from the video file stream

    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        start = time.time()
        image = frame.array
        detect = not detect
        if not detect:
            img2 = cv2.resize(image, (640, 480))
            cv2.imshow("frame", img2)
            continue
        #image = np.asarray(image)
        #cv2.imshow("input", image)
     
        #blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416),
                                     #swapRB=True, crop=False)
        
        blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416),
                                     swapRB=True, crop=False)
        net.setInput(blob)
        layerOutputs = net.forward(ln)

        # initialize our lists of detected bounding boxes, confidences,
        # and class IDs, respectively
        boxes = []
        confidences = []
        classIDs = []

        # loop over each of the layer outputs
        for output in layerOutputs:
            # loop over each of the detections
            for detection in output:
                # extract the class ID and confidence (i.e., probability)
                # of the current object detection
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]

                if confidence > confidence_a:
                    box = detection[0:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box.astype("int")

                    # use the center (x, y)-coordinates to derive the top
                    # and and left corner of the bounding box
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))

                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(classID)

        idxs = cv2.dnn.NMSBoxes(boxes, confidences, confidence_a,
                               threshold_a)

        # ensure at least one detection exists
        if len(idxs) > 0:
            # loop over the indexes we are keeping
            for i in idxs.flatten():
                # extract the bounding box coordinates
                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])
                
                color = [int(c) for c in COLORS[classIDs[i]]]
                cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)
                text = "{}: {:.4f}".format(LABELS[classIDs[i]],
                                           confidences[i])
                cv2.putText(image, text, (x, y - 5),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

        image_width = 640
        image_height = 480
        img2 = cv2.resize(image, (image_width, image_height))
        frame_data = count_classes(classIDs)

        img2 = cv2.putText(img2, "No of person: " + str(frame_data['no_of_people']),
                           (30, image_height - 170), cv2.FONT_HERSHEY_TRIPLEX, 0.6, (150, 100, 50), 2, cv2.LINE_AA)
        img2 = cv2.putText(img2,
                           "No of people with both hard hat and vest: " + str(frame_data['no_of_people_wearing_both'])
                           , (30, image_height - 140), cv2.FONT_HERSHEY_TRIPLEX, 0.6, (150, 100, 50), 2, cv2.LINE_AA)
        img2 = cv2.putText(img2, "No of people with hard Hats only: " + str(
            frame_data['no_of_people_wearing_only_hard_hats']),
                           (30, image_height - 110), cv2.FONT_HERSHEY_TRIPLEX, 0.6, (150, 100, 50), 2, cv2.LINE_AA)
        img2 = cv2.putText(img2, "No of people with vest only: " + str(frame_data['no_of_people_wearing_only_hvv']),
                           (30, image_height - 80), cv2.FONT_HERSHEY_TRIPLEX, 0.6, (150, 100, 50), 2, cv2.LINE_AA)
        img2 = cv2.putText(img2, "No of people with None: " + str(frame_data['no_of_people_wearing_none']),
                           (30, image_height - 50),
                           cv2.FONT_HERSHEY_TRIPLEX, 0.6, (150, 100, 50), 2, cv2.LINE_AA)

        cv2.imshow("frame", img2)

        key = cv2.waitKey(1)
        rawCapture.truncate(0)
        if key == 27:
            break
        end = time.time()
        print('FPS: ', 1 / (end - start))
        #print(end-start)

    cv2.destroyAllWindows()


def video_detection(net, ln, confidence_a, threshold_a, LABELS, video_file):
    W = 320
    H = 240
    
    cap = cv2.VideoCapture(video_file)
    ending_frame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    input_fps = cap.get(cv2.CAP_PROP_FPS)
    cap.grab()
    ret, frame = cap.read()
    resized_frame = cv2.resize(frame, dsize=(W, H))
    size = (resized_frame.shape[:2])
    video_output = 'output.mp4'
    out = cv2.VideoWriter(video_output, cv2.VideoWriter_fourcc(*'DIVX'), 15, size)
    output_fps = input_fps / 1
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(video_output, fourcc, output_fps, (resized_frame.shape[1], resized_frame.shape[0]))
    video_output = "output.mp4"
    frame_count = 0
    detect = False
    frame_counter = 0
    
    while (cap.isOpened()) and ret is True:
        start = time.time()
        ret, frame = cap.read()
        frame_counter += 1
        if (frame_counter - 1) % 3 == 0:
            img2 = cv2.resize(frame, (640,480))
            cv2.imshow("frame", img2)
            continue
    
        image = cv2.resize(frame, dsize=(W, H))
        blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416),
                                     swapRB=True, crop=False)
        net.setInput(blob)
        layerOutputs = net.forward(ln)

        # initialize our lists of detected bounding boxes, confidences,
        # and class IDs, respectively
        boxes = []
        confidences = []
        classIDs = []

        # loop over each of the layer outputs
        for output in layerOutputs:
            # loop over each of the detections
            for detection in output:

                # extract the class ID and confidence (i.e., probability)
                # of the current object detection
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]

                if confidence > confidence_a:
                    box = detection[0:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box.astype("int")

                    # use the center (x, y)-coordinates to derive the top
                    # and and left corner of the bounding box
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))

                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(classID)

        idxs = cv2.dnn.NMSBoxes(boxes, confidences, confidence_a,
                               threshold_a)

        # ensure at least one detection exists
        if len(idxs) > 0:
            # loop over the indexes we are keeping
            for i in idxs.flatten():
                # extract the bounding box coordinates
                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])
                
                color = [int(c) for c in COLORS[classIDs[i]]]
                cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)
                text = "{}: {:.4f}".format(LABELS[classIDs[i]],
                                           confidences[i])
                cv2.putText(image, text, (x, y - 5),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
        image_width = 640
        image_height = 480
        img2 = cv2.resize(image, (image_width, image_height))
        frame_data = count_classes(classIDs)

        img2 = cv2.putText(img2, "No of person: " + str(frame_data['no_of_people']),
                                    (30, image_height - 170), cv2.FONT_HERSHEY_TRIPLEX, 0.6, (150, 100, 50), 2, cv2.LINE_AA)
        img2 = cv2.putText(img2, "No of people with both hard hat and vest: " + str(frame_data['no_of_people_wearing_both'])
                           , (30, image_height - 140), cv2.FONT_HERSHEY_TRIPLEX, 0.6, (150, 100, 50), 2, cv2.LINE_AA)
        img2 = cv2.putText(img2, "No of people with hard Hats only: " + str(frame_data['no_of_people_wearing_only_hard_hats']),
                                    (30, image_height - 110), cv2.FONT_HERSHEY_TRIPLEX, 0.6, (150, 100, 50), 2, cv2.LINE_AA)
        img2 = cv2.putText(img2, "No of people with vest only: " + str(frame_data['no_of_people_wearing_only_hvv']),
                                    (30, image_height - 80), cv2.FONT_HERSHEY_TRIPLEX, 0.6, (150, 100, 50), 2, cv2.LINE_AA)
        img2 = cv2.putText(img2, "No of people with None: " +str(frame_data['no_of_people_wearing_none']), (30, image_height - 50),
                                    cv2.FONT_HERSHEY_TRIPLEX, 0.6, (150, 100, 50), 2, cv2.LINE_AA)

        cv2.imshow("frame", img2)
        frame_count += 1

        key = cv2.waitKey(1)
        end = time.time()
        print('fps:', 1/(end-start))
            
        if key == 27:
            break
        #print(end-start)

    cv2.destroyAllWindows()


def count_classes(classIDs):
    no_of_hard_hats = classIDs.count(0)
    no_of_hvv = classIDs.count(1)
    no_of_people = classIDs.count(2)
    no_of_people_wearing_both = min(no_of_hard_hats, no_of_hvv)
    no_of_people_wearing_only_hard_hats = max(0, no_of_hard_hats - no_of_people_wearing_both)
    no_of_people_wearing_only_hvv = max(0, no_of_hvv - no_of_people_wearing_both)
    no_of_people_wearing_none = max(0, no_of_people - (no_of_people_wearing_both + no_of_people_wearing_only_hard_hats + no_of_people_wearing_only_hvv))

    return {
        "no_of_people": no_of_people,
        'no_of_people_wearing_both': no_of_people_wearing_both,
        "no_of_people_wearing_only_hard_hats": no_of_people_wearing_only_hard_hats,
        "no_of_people_wearing_only_hvv": no_of_people_wearing_only_hvv,
        "no_of_people_wearing_none": no_of_people_wearing_none
    }



def main():
    parser = argparse.ArgumentParser(description="Hardhat and Vest Detection", add_help=True)
    parser.add_argument("--video_file_name", type=str, required=False,default="./data/input/input.mp4", help="path to video file, or camera device, i.e /dev/video1")
    parser.add_argument("--camera_id", type=str, required=False,default=None, help="camera identifier")
    args = parser.parse_args()
    
    confidence_a = 0.3
    threshold_a = 0.2
    yolo_label = './package/obj.names'
    # load the COCO class labels our YOLO model was trained on
    # labelsPath = os.path.sep.join(yolo_label)
    LABELS = open(yolo_label).read().strip().split("\n")


    # derive the paths to the YOLO weights and model configuration
    yolo_weight = './package/obj_43600.weights'
    yolo_config = './package/obj.cfg'
    weightsPath = yolo_weight
    configPath = yolo_config

    # load our YOLO object detector trained on COCO dataset (80 classes)
    # and determine only the *output* layer names that we need from YOLO
    print("[INFO] loading YOLO from disk...")
    net = cv2.dnn.readNetFromDarknet(configPath, weightsPath)
    ln = net.getLayerNames()
    ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    
    if args.camera_id is not None:
        picam_detection(net, ln, confidence_a, threshold_a, LABELS)
    else:
        video_detection(net, ln, confidence_a, threshold_a, LABELS, args.video_file_name)



if __name__ == '__main__':
    main()
